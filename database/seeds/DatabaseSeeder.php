<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //create dummy user
        /* 
        factory('App\User', 100)->create(); 
        */
        
        //create dummy user with posts
        factory('App\User', 10)->create()->each(function($user){
            $user->posts()->save( factory("App\Post")->make());
        });
        // $this->call(UserSeeder::class);

        //to run seeder, artisan command => php artisan db:seed
    }
}
