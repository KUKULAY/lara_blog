<!-- Header Menu
============================================= -->
<header id="header" class="full-header">

    <div id="header-wrap">

        <div class="container clearfix">

            <div class="header-row">

                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                <!-- Logo
				============================================= -->
                <div id="logo">
                    <a href="index.html" class="standard-logo" data-dark-logo="images/logo-dark.png"><img
                            src="{{ asset('images/572.png') }}" alt="Canvas Logo"></a>
                    <a href="index.html" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img
                            src="{{ asset('images/572.png') }}" alt="Canvas Logo"></a>
                </div><!-- #logo end -->

                <div class="header-misc">
                    
                    <x-profile_nav></x-profile_nav>

                </div>

                <!-- Primary Navigation
				============================================= -->
                <nav class="primary-menu">

                    <ul class="menu-container">
                        @if (auth()->check() && auth()->user()->userHasRole('admin'))
                            <li class="menu-item">
                                <a class="menu-link" href="{{ route('admin.dashboard') }}">
                                    <div>Admin Panel</div>
                                </a>
                                <ul class="sub-menu-container">
                                    <li class="menu-item">
                                        <a class="menu-link" href="{{ route('admin.dashboard') }}"><div>User List</div></a>
                                    </li>
                                   {{--  <li class="menu-item">
                                        <a class="menu-link" href="{{ route('user.create') }}"><div>Create New User</div></a>
                                    </li> --}}
                                    <li class="menu-item">
                                        <a class="menu-link" href="{{ route('role.index') }}"><div>Role</div></a>
                                    </li>
                                    <li class="menu-item">
                                        <a class="menu-link" href="{{ route('user.create') }}"><div>Permissions</div></a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        
                        <li class="menu-item">
                            <a class="menu-link" href="{{ route('post.newfeeds') }}">
                                <div>New Feeds</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="{{ route('post.create') }}">
                                <div>Create Post</div>
                            </a>
                        </li>
                        <li class="menu-item">
                            <a class="menu-link" href="{{ route('about.overview') }}">
                                <div>About Blog</div>
                            </a>
                        </li>
                    </ul>

                    <!-- Top Search
					============================================= -->
                    {{-- <div id="top-account">
						@if (Auth::check())
						<a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    @else
                    <a href="{{ route('login') }}"><i class="icon-line2-user mr-1 position-relative"
                            style="top: 1px;"></i><span
                            class="d-none d-sm-inline-block font-primary t500">Login</span></a>
                    @endif
            </div> --}}
            <!-- #top-search end -->


            </nav>

        </div>

    </div>

    </div>

</header><!-- #header end -->