<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	@include('layouts.header')

</head>

<body class="stretched">

    <!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">

        @include('layouts.menu')

        
        <!-- Content
        ============================================= -->
		<section id="content">

			<div class="content-wrap">

				
                @yield('content')


			</div>

		</section><!-- #content end -->

        @include('layouts.footer')

		@yield('specific_scripts')
		
    </div>

</body>

</html>