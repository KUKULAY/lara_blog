<!-- Stylesheets
============================================= -->
    
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('css/swiper.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('css/dark.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('css/font-icons.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('css/animate.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}" type="text/css" />

<link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css" />

<meta name="viewport" content="width=device-width, initial-scale=1" />

<!-- Bootstrap File Upload CSS -->
<link rel="stylesheet" href="{{ asset('css/components/bs-filestyle.css') }}" type="text/css" />
<link rel="stylesheet" href="{{ asset('css/components/select-boxes.css') }}" type="text/css" />

@yield('name')

<!-- External JavaScripts
============================================= -->
<script src="{{ asset('js/jquery.js') }}"></script>

@yield('individual_css')