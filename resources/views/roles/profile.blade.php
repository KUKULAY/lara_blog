@extends('layouts/default')

<x-document_title>
    User Profile
</x-document_title>

@section("content")

    <div class="container clearfix">
        <div class="row clearfix">

            <div class="col-md-9">

                @component('components/flash_message')

                @endcomponent

                <img src="{{$user->avatar}}" class="profile-image alignleft img-circle img-thumbnail my-0" alt="Avatar" style="max-width: 100px;">

                <div class="heading-block border-0">
                    <h3>{{ $user->name }} </h3>
                    @if ( auth()->user()->can('update', $user) )
                        <a href="{{ route('user.edit', $user->id) }}"><span title="Edit Profile" >Edit Profile Details &nbsp; <i class="icon-user-edit" ></i></span></a>  
                    @endif
                </div>

                <div class="clear"></div>

                <div class="row clearfix">

                    <div class="col-lg-12">

                        <div class="tabs tabs-alt clearfix" id="tabs-profile">

                            <ul class="tab-nav clearfix">
                                <li><a href="#tab-posts"> Posts</a></li>
                                <li><a href="#tab-replies"> Followers</a></li>
                                <li><a href="#tab-connections"> Followings</a></li>
                            </ul>

                            <div class="tab-container">

                                <div class="tab-content clearfix" id="tab-posts">

                                    <!-- Posts
                                    ============================================= -->
                                    <div class="row gutter-40 posts-md mt-4">

                                        @foreach ($user->posts as $post)
                                            <x-post_card>
                                                @slot('post_title')
                                                    {{ $post->title }}
                                                @endslot
                                                @slot('post_created_date')
                                                    {{ $post->created_at->diffForHumans() }}
                                                @endslot
                                                @slot('post_media')
                                                    {{ $post->media }}
                                                @endslot
                                                @slot('post_id')
                                                    {{ $post->id }}
                                                @endslot
                                                @slot('editable')
                                                    {{ auth()->user()->can('update', $post)}}
                                                @endslot
                                                @slot('deletable')
                                                    {{ auth()->user()->can('delete', $post)}}
                                                @endslot
                                                {{ $post->content }}

                                            </x-post_card>
                                        @endforeach
                                        

                                    </div>

                                </div>
                                <div class="tab-content clearfix" id="tab-replies">

                                    here will show user's followers

                                </div>
                                <div class="tab-content clearfix" id="tab-connections">

                                    Here will show user following


                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="w-100 line d-block d-md-none"></div>

            <div class="col-md-3">

                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Profile</div><i class="icon-user"></i></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Servers</div><i class="icon-laptop2"></i></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Messages</div><i class="icon-envelope2"></i></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Billing</div><i class="icon-credit-cards"></i></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Settings</div><i class="icon-cog"></i></a>
                    <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between"><div>Logout</div><i class="icon-line2-logout"></i></a>
                </div>

                <div class="fancy-title topmargin title-border">
                    <h4>About Me</h4>
                </div>

                <p>{{ $user->user_bio }}</p>

                <div class="fancy-title topmargin title-border">
                    <h4>Social Profiles</h4>
                </div>

                <a href="#" class="social-icon si-facebook si-small si-rounded si-light" title="Facebook">
                    <i class="icon-facebook"></i>
                    <i class="icon-facebook"></i>
                </a>

                <a href="#" class="social-icon si-gplus si-small si-rounded si-light" title="Google+">
                    <i class="icon-gplus"></i>
                    <i class="icon-gplus"></i>
                </a>

                <a href="#" class="social-icon si-dribbble si-small si-rounded si-light" title="Dribbble">
                    <i class="icon-dribbble"></i>
                    <i class="icon-dribbble"></i>
                </a>

                <a href="#" class="social-icon si-flickr si-small si-rounded si-light" title="Flickr">
                    <i class="icon-flickr"></i>
                    <i class="icon-flickr"></i>
                </a>

                <a href="#" class="social-icon si-linkedin si-small si-rounded si-light" title="LinkedIn">
                    <i class="icon-linkedin"></i>
                    <i class="icon-linkedin"></i>
                </a>

                <a href="#" class="social-icon si-twitter si-small si-rounded si-light" title="Twitter">
                    <i class="icon-twitter"></i>
                    <i class="icon-twitter"></i>
                </a>

            </div>

        </div>
    </div>
    
@endsection