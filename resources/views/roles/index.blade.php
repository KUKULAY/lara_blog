@extends('layouts/default')

<x-document_title>
    Dashboard
</x-document_title>

@section("content")
    
    

    <div class="container clearfix">

        <div class="heading-block center">
            <h2>Roles.</h2>
            <span>This blog have these roles:</span>
        </div>


        <div class="row">
            <div class="col-lg-4 order-lg-1 mb-2">
                    @component('components/flash_message')
                    @endcomponent

                    <form class="row mb-0" id="create-role" action="{{ route('role.store') }}" method="post" >
                        @csrf
                        <div class="col-10 form-group mb-2">
                            <input type="text" name="name" class="form-control required" value="" placeholder="Enter Role Name">
                        </div>
                        <div class="col-10">
                            <button type="submit" name="create-role-submit" class="button button-3d button-rounded gradient-ocean button-medium btn-block">Create</button>
                        </div>
                    </form>

                    <form class="row mb-0" id="update-role" action="" method="post" style="display: none">
                        @csrf
                        @method('PATCH')
                        <div class="col-10 form-group mb-2">
                            <input type="text" name="name" id="name" value="" class="form-control required" value="" placeholder="Enter Role Name">
                        </div>
                        <div class="col-10">
                            <button type="submit" name="update-role-submit" class="button button-3d button-rounded gradient-ocean button-medium btn-block">Update</button>
                        </div>
                    </form>
            </div>
            <div class="col-lg-8 order-lg-1 mb-4">
                <!--  Data Table  -->
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($roles as $role)
                                <tr>
                                    <td>{{ $role->id }}</td>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->slug }}</td>
                                    <td>{{ $role->created_at->diffForHumans() }}</td>
                                    <td>{{ $role->updated_at->diffForHumans() }}</td>
                                    <td>
                                        <a onclick="change_form({{$role->id}}, '{{$role->name}}')"><i class="icon-edit" title="Edit"></i></a>
                                        <form onSubmit="if(!confirm('Do you really want to delete Role?')){return false;}" method="post" action="{{ route('role.destroy', $role->id)}}" enctype="multipart/form-data" style="display: inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn" ><i class="icon-remove-sign" title="Delete"></i></button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    @section('individual_css')
        <!-- Bootstrap Data Table Plugin -->
        <link rel="stylesheet" href="{{ asset('css/components/bs-datatable.css') }}" type="text/css" />
    @endsection


    @section('individual_js')
        <!-- Bootstrap Data Table Plugin -->
        <script src="{{ asset('js/components/bs-datatable.js') }}"></script>    
    @endsection
   
    <script>

		$(document).ready(function() {
			$('#datatable1').dataTable();
		});

        function change_form(id, name){
            $("#create-role").css('display','none');
            $("#update-role").css('display','block');
            $("#name").val(name);
            $("#update-role").attr('action', "role/"+id+"/update")
        }
    </script>
    
    <style>
        th {
            background-color:#1798bd;
            color: white;
        } 
    </style>
@endsection
