@extends('layouts/default')

<x-document_title>
    Dashboard
</x-document_title>

@section("content")
    
    @component('components/flash_message')
    @endcomponent

    <div class="container clearfix">

        <div class="heading-block center">
            <h2>Bloggers.</h2>
            <span>All developers fall in love with Coding.</span>
        </div>

        <!--  Data Table  -->
        <div class="table-responsive">
            <table id="datatable1" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>User Bio</th>
                        <th>Created</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->user_bio }}</td>
                            <td>{{ $user->created_at->diffForHumans() }}</td>
                            <td>
                                <i class="icon-edit" title="Edit"></i>
                                <form onSubmit="if(!confirm('Do you really want to delete User?')){return false;}" method="post" action="{{ route('user.destroy', $user->id)}}" enctype="multipart/form-data" style="display: inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn" ><i class="icon-remove-sign" title="Delete"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>

    @section('individual_css')
        <!-- Bootstrap Data Table Plugin -->
        <link rel="stylesheet" href="{{ asset('css/components/bs-datatable.css') }}" type="text/css" />
    @endsection


    @section('individual_js')
        <!-- Bootstrap Data Table Plugin -->
        <script src="{{ asset('js/components/bs-datatable.js') }}"></script>    
    @endsection
   
    <script>

		$(document).ready(function() {
			$('#datatable1').dataTable();
		});

    </script>
    
    <style>
        th {
            background-color:#1abc9b;
            color: white;
        } 
    </style>
@endsection
