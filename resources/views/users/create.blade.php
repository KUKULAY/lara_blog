@extends('layouts/default')

@section("content")

    <x-document_title>
        Create Profile
    </x-document_title>

    

    @component('components/flash_message')

    @endcomponent

    <div class="container clearfix">
        <x-page_title>
            Crate User Profile
        </x-page_title>
        <div class="" data-alert-type="inline">

            <div class="row shadow bg-light border  justify-content-center">

                <div class="col-xs-3 col-md-3 p-5 dark" style="">
                    <div  class=" clearfix">
                    <img src="{{ asset('images/user-default.png') }}" class="profile-image aligncenter  mt-4" alt="Avatar" style="width:200px; height:200px; max-width: 400px;">

                    </div>
                    
                </div>

                <div class="col-xs-6 col-md-8  p-5">
                    <form class="row mb-0" id="user-form" method="post" action="{{route('user.store')}}"  enctype="multipart/form-data">
                        @csrf
                        <div class="form-process"></div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-sm-2 col-form-label">
                                    <label for="user-form-name">Name:</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" id="user-form-name" name="name" class="form-control required" placeholder="Enter user name">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-sm-2 col-form-label">
                                    <label for="post-form-content">Image:</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="file" id="post-form-avatar" name="avatar" multiple class="file file-loading" data-show-preview="false">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-sm-2 col-form-label">
                                    <label for="user-form-email">Email:</label>
                                </div>
                                <div class="col-sm-10">
                                <input type="email" id="user-form-email" name="email" class="form-control required" placeholder="Enter user email">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-sm-2 col-form-label">
                                    <label for="user-form-user_bio">About Me:</label>
                                </div>
                                <div class="col-sm-10">
                                    <textarea class="required form-control textarea-message" id="user-form-user_bio"  name="user_bio" rows="6" cols="30"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-sm-2 col-form-label">
                                    <label for="user-form-password">Password:</label>
                                </div>
                                <div class="col-sm-10">
                                <input type="password" id="user-form-password" name="password" class="form-control " value="" placeholder="Enter Password">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-sm-2 col-form-label">
                                    <label for="user-form-password_confirm">Confirm Password:</label>
                                </div>
                                <div class="col-sm-10">
                                <input type="password" id="user-form-password_confirm" name="password_confirm" class="form-control " value="" placeholder="Enter Confirm Password">
                                </div>
                            </div>
                        </div>

                        <div class="col-12 d-flex justify-content-end align-items-center">
                            <button type="button" onclick="window.location='{{route('admin.dashboard')}}'" id="user-form-btn-cancel" class="btn btn-secondary">Cancel</button>
                            <button type="submit" id="user-form-btn-create" class="btn btn-success ml-2">Create</button>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>


    <style>
        .form-group > label.error {
            display: block !important;
            text-transform: none;
        }

        .form-group input.valid ~ label.error,
        .form-group input[type="text"] ~ label.error,
        .form-group input[type="textarea"] ~ label.error,
        .form-group select ~ label.error { display: none !important; }

    </style>
    <script>
        function change_avatar(value){
            $("#name").val(value);
        }
    </script>
@endsection

