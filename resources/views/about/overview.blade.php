@extends('layouts/default')

<x-document_title>
    About project
</x-document_title>

@section("content")

    <div class="container clearfix">

        <!-- Portfolio Single Image
        ============================================= -->
        <div class="portfolio-single-image portfolio-single-image-full" style="background-image: url('https://images.unsplash.com/photo-1499951360447-b19be8fe80f5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80');" data-bottom-top="background-position:0px 300px;" data-top-bottom="background-position:0px -300px;"></div><!-- .portfolio-single-image end -->

    </div>

    <div class="container clearfix">

        <div class="row col-mb-50">

            <div class="col-sm-6 col-lg-4">
                <div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect">
                    <div class="fbox-icon">
                        <a href="#"><i class="icon-laptop-code i-alt"></i></a>
                    </div>
                    <div class="fbox-content">
                        <h3>Visual Studio Code<span class="subtitle">Lightweight but powerful source code editor</span></h3>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-4">
                <div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect">
                    <div class="fbox-icon">
                        <a href="#"><i class="icon-mixcloud i-alt"></i></a>
                    </div>
                    <div class="fbox-content">
                        <h3>Git<span class="subtitle">Free &amp; open source distributed VCS</span></h3>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-4">
                <div class="feature-box fbox-center fbox-outline fbox-lg fbox-effect">
                    <div class="fbox-icon">
                        <a href="#"><i class="icon-eye i-alt"></i></a>
                    </div>
                    <div class="fbox-content">
                        <h3>Web Tinker<span class="subtitle">Great way to tinker with application in the terminal</span></h3>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="container clearfix">
        <!-- Content
            ============================================= -->
            <div class="divider divider-sm divider-center"><i class="icon-circle"></i></div>
            
            {{-- <div class="heading-block center">
                <h2>Laravel Techniques List.</h2>
                <span>I learned and implemented these useful &amp; Flexible Features</span>
            </div> --}}

            <div id="section-features" class="heading-block text-center page-section">
                <h2>Laravel Features</h2>
                <span>Features that have used at this blog project</span>
            </div>

            <div class="row justify-content-center col-mb-50 mb-0">
                <div class="col-md-6 col-lg-4">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn">
                            <a href="#"><img src="{{ asset('images/icons/features/responsive.png') }}" alt="MVC Architecture Support"></a>
                        </div>
                        <div class="fbox-content">
                            <h3>MVC Architecture Support</h3>
                            <p>Powerful Layout with Responsive functionality that can be adapted to any screen size. Resize browser to view.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn" data-delay="200">
                            <a href="#"><img src="{{ asset('images/icons/features/retina.png') }}" alt="Artisan"></a>
                        </div>
                        <div class="fbox-content">
                            <h3>Artisan</h3>
                            <p>Looks beautiful &amp; ultra-sharp on Retina Screen Displays. Retina Icons, Fonts &amp; all others graphics are optimized.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn" data-delay="400">
                            <a href="#"><img src="{{ asset('images/icons/features/performance.png') }}" alt="Security"></a>
                        </div>
                        <div class="fbox-content">
                            <h3>Security</h3>
                            <p>Canvas includes tons of optimized code that are completely customizable and deliver unmatched fast performance.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn" data-delay="600">
                            <a href="#"><img src="{{ asset('images/icons/features/flag.png') }}" alt="Blade Template Engine"></a>
                        </div>
                        <div class="fbox-content">
                            <h3>Blade Template Engine</h3>
                            <p>Laravel framework is highly acknowledged for its built-in lightweight templates which can be used to create wonderful layouts using dynamic content seeding. In addition to this, it provides multiple widgets incorporating CSS and JS code with robust structures. Laravel templates are innovatively designed to create simple as well as complex layouts with distinctive sections.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn" data-delay="800">
                            <a href="#"><img src="{{ asset('images/icons/features/tick.png') }}" alt="Unit Testing"></a>
                        </div>
                        <div class="fbox-content">
                            <h3>Unit Testing</h3>
                            <p>Change your Website's Primary Scheme instantly by simply adding the dark class to the body.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn" data-delay="1000">
                            <a href="#"><img src="{{ asset('images/icons/features/tools.png') }}" alt="Eloquent ORM"></a>
                        </div>
                        <div class="fbox-content">
                            <h3>Eloquent ORM</h3>
                            <p>Use any Font you like from Google Web Fonts, Typekit or other Web Fonts. They will blend in perfectly.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn" data-delay="1200">
                            <a href="#"><img src="{{ asset('images/icons/features/map.png') }}" alt="Database Migration System"></a>
                        </div>
                        <div class="fbox-content">
                            <h3>Database Migration System</h3>
                            <p>Powerful Layout with Responsive functionality that can be adapted to any screen size. Resize browser to view.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn" data-delay="1400">
                            <a href="#"><img src="{{ asset('images/icons/features/seo.png') }}" alt="Horizon"></a>
                        </div>
                        <div class="fbox-content">
                            <h3>Horizon</h3>
                            <p>Looks beautiful &amp; ultra-sharp on Retina Screen Displays. Retina Icons, Fonts &amp; all others graphics are optimized.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn" data-delay="1600">
                            <a href="#"><img src="{{ asset('images/icons/features/support.png') }}" alt="Libraries & Modular"></a>
                        </div>
                        <div class="fbox-content">
                            <h3>Libraries & Modular</h3>
                            <p>Canvas includes tons of optimized code that are completely customizable and deliver unmatched fast performance.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn" data-delay="1000">
                            <a href="#"><img src="{{ asset('images/icons/features/tools.png') }}" alt="Routes, Middleware, Policy"></a>
                        </div>
                        <div class="fbox-content">
                            <h3>Routes, Middleware, Policy</h3>
                            <p>Use any Font you like from Google Web Fonts, Typekit or other Web Fonts. They will blend in perfectly.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn">
                            <a href="#"><img src="{{ asset('images/icons/features/responsive.png') }}" alt="Session"></a>
                        </div>
                        <div class="fbox-content">
                            <h3>Session</h3>
                            <p>Powerful Layout with Responsive functionality that can be adapted to any screen size. Resize browser to view.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="feature-box fbox-plain">
                        <div class="fbox-icon" data-animate="bounceIn" data-delay="800">
                            <a href="#"><img src="{{ asset('images/icons/features/tick.png') }}" alt="Caching"></a>
                        </div>
                        <div class="fbox-content">
                            <h3>Caching</h3>
                            <p>Change your Website's Primary Scheme instantly by simply adding the dark class to the body.</p>
                        </div>
                    </div>
                </div>

            </div>

            {{-- <div class="divider divider-sm divider-center"><i class="icon-circle"></i></div> --}}
            
    </div>

    <div class="section parallax mb-50 border-0 dark" style="height: 450px; padding: 120px 0;">

        <div class="vertical-middle center" style="z-index: 2;">
            <div class="container clearfix" data-animate="fadeInUp">
                <div class="heading-block border-bottom-0 mb-0">
                    <h2 style="font-size: 42px;">Beautiful HTML5 Videos</h2>
                    <span class="d-none d-lg-block">Looks beautiful &amp; ultra-sharp on Retina Screen Displays. Powerful Layout with Responsive functionality that can be adapted to any screen size.</span>
                </div>
                <a href="#" class="button button-border button-rounded button-white button-light button-large ml-0 mb-0" style="margin-top: 40px;">Show More</a>
            </div>
        </div>

        <div class="video-wrap" style="z-index: 1;">
            <video poster="images/videos/explore-poster.jpg" preload="auto" loop autoplay muted>
                <source src='images/videos/explore.mp4' type='video/mp4' />
                <source src='images/videos/explore.webm' type='video/webm' />
            </video>
            <div class="video-overlay" style="background-color: rgba(0,0,0,0.1);"></div>
        </div>

    </div>

    <div class="container clearfix">

        <div class="pricing-box pricing-extended row align-items-stretch mb-5 mx-0">
            <div class="pricing-desc col-lg-9 p-4">
                <div class="pricing-title">
                    <h3>How many Themes can you Download today?</h3>
                </div>
                <div class="pricing-features pb-0">
                    <ul class="row">
                        <li class="col-md-6"><i class="icon-desktop mr-2"></i> Ultra Responsive Layouts</li>
                        <li class="col-md-6"><i class="icon-eye-open mr-2"></i> Retina Ready Designs</li>
                        <li class="col-md-6"><i class="icon-beaker mr-2"></i> Advanced Admin Panel</li>
                        <li class="col-md-6"><i class="icon-magic mr-2"></i> Tons of Customization Options</li>
                        <li class="col-md-6"><i class="icon-font mr-2"></i> Support for Custom Fonts</li>
                        <li class="col-md-6"><i class="icon-stack3 mr-2"></i> Premium Sliders Included</li>
                        <li class="col-md-6"><i class="icon-file2 mr-2"></i> Photoshop Source Files Included</li>
                        <li class="col-md-6"><i class="icon-support mr-2"></i> 24x7 Priority Email Support</li>
                    </ul>
                </div>
            </div>

            <div class="pricing-action-area col-lg d-flex flex-column justify-content-center">
                <div class="entry-image alignleft">
                    <a href="#"><img src="{{ asset('images/poe4.jpg') }}" alt="Blog Single"></a>
                    <h4 class="mt-3 mb-0" data-animate="fadeInUp">Welcome to our blog!</h4>
                    <p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200">I am Poe, web developer of this blog. Thanks for your visting to here.</p>
                </div>
            </div>
        </div>

    </div>
    
@endsection
