@extends('layouts/default')

@section("content")

    <x-document_title>
        Edit Post
    </x-document_title>

    

    @component('components/flash_message')

    @endcomponent

    <div class="container clearfix">
        <x-page_title>
            Edit Post
        </x-page_title>

        <div class="" data-alert-type="inline">

            <div class="row shadow bg-light border">

                <div class="col-lg-4 dark" style="background:  url('{{ $post->media }}') center center / cover; min-height: 400px; max-height: 400px">
                    
                </div>

                <div class="col-lg-8 p-5">
                    <form class="row mb-0" id="post-form" method="post" action="{{route('post.update', $post->id)}}"  enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <div class="form-process"></div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-sm-2 col-form-label">
                                    <label for="post-form-title">Title:</label>
                                </div>
                                <div class="col-sm-10">
                                <input type="text" id="post-form-title" name="title" class="form-control required" value="{{ $post->title }}" placeholder="Enter Post Title">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-sm-2 col-form-label">
                                    <label for="post-form-content">Content:</label>
                                </div>
                                <div class="col-sm-10">
                                    <textarea class="required form-control textarea-message" id="post-form-content"  name="content" rows="6" cols="30">{{ $post->content }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <div class="row">
                                <div class="col-sm-2 col-form-label">
                                    <label for="post-form-content">Image:</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="file" id="post-form-media" name="media" multiple class="file file-loading" data-show-preview="false">
                                </div>
                            </div>
                        </div>

                        <div class="col-12 d-flex justify-content-end align-items-center">
                            <button type="button" onclick="window.location='{{route('post.index')}}'" id="post-form-btn-cancel" class="btn btn-secondary">Cancel</button>
                            <button type="submit" id="post-form-btn-update" class="btn btn-success ml-2">Update</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <style>
        .form-group > label.error {
            display: block !important;
            text-transform: none;
        }

        .form-group input.valid ~ label.error,
        .form-group input[type="text"] ~ label.error,
        .form-group input[type="textarea"] ~ label.error,
        .form-group select ~ label.error { display: none !important; }

    </style>

@endsection

