@extends('layouts/default')

<x-document_title>
    Blog Grid
</x-document_title>

@section("content")

    @component('components/flash_message')
    @endcomponent

    <div class="container clearfix">
        <!-- Posts
            ============================================= -->
        {{-- <div id="posts" class="post-grid grid-container grid-3 clearfix" data-layout="fitRows"> --}}
        <div id="posts" class="post-grid row grid-container gutter-40 clearfix" data-layout="fitRows">
            @foreach ($posts as $post)
            <div class="entry col-md-4 col-sm-6 col-12">
                <div class="grid-inner">
                    <div class="entry-image">
                        <a href="" data-lightbox="image"><img class="image_fade" src="{{ $post->media }}"
                                alt="Standard Post with Image"></a>
                    </div>
                    <div class="entry-title">
                        <h2><a href="{{ route('post.show', $post->id) }}">{{ Str::limit($post->title,'30', '......') }}</a></h2>
                    </div>
                    
                    <div class="entry-meta">
                        <ul>
                            <li><i class="icon-calendar3"></i> {{ $post->created_at->diffForHumans() }}</li>
                        <li><a href="{{ route('user.show', $post->user->id) }}"><i class="icon-user"></i> {{ $post->user->name }}</a></li>
                        </ul>
                    </div>
                    <div class="entry-content">
                        <p>{!! Str::limit($post->content,'150', '......') !!}</p>

                        <a href="{{ route('post.show', $post->id) }}"
                            class="button button-3d button-rounded button-green button-small"><i class="icon-hand-right"></i>Read
                            More</a>
                        
                        @can('view', $post) {{-- checking post's owner? by using policy --}}
                            {{-- Edit --}}
                            <a href="{{ route('post.edit', $post->id) }}"
                                class="button button-3d button-rounded button-blue button-small"><i class="icon-edit"></i>Edit
                                </a>
                            {{-- Delete --}}
                            @component('components/form_delete')
                                @slot('delete_url')
                                {{ route('post.destroy', $post->id) }}
                                @endslot
                            @endcomponent
                        @endcan
                    </div>
                </div>
            </div>
            @endforeach

            
        </div><!-- #posts end -->
    </div>

    <!-- Pagination
        ============================================= -->
    <ul class="pagination nobottommargin">
        {{ $posts->links() }}
    </ul>

    <style>
        .image_fade {
            max-height: 250px !important;
        }
    </style>
@endsection
