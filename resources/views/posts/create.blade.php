@extends('layouts/default')

@section("content")

<x-document_title>
	Create New Post
</x-document_title>



@component('components/flash_message')

@endcomponent

<div class="container clearfix">
    <x-page_title>
        Create Post
    </x-page_title>
    <div class="" data-alert-type="inline">

        <div class="form-result"></div> {{-- to receive the Success/Error Message from PHP --}}

        <div class="row shadow bg-light border">

            <div class="col-lg-4 dark" style="background: linear-gradient(rgba(0,0,0,.8), rgba(0,0,0,.2)), url('https://images.unsplash.com/photo-1511809870860-4d2806eb1908?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=668&q=80') center center / cover; min-height: 400px; max-height: 400px">
                
            </div>

            <div class="col-lg-8 p-5">
                <form class="row mb-0" id="post-form" method="post" action="{{route('post.store')}}"  enctype="multipart/form-data">
                    @csrf
                    <div class="form-process"></div>
                    <div class="col-12 form-group">
                        <div class="row">
                            <div class="col-sm-2 col-form-label">
                                <label for="post-form-title">Title:</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="text" id="post-form-title" name="title" class="form-control required" value="" placeholder="Enter Post Title">
                            </div>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <div class="row">
                            <div class="col-sm-2 col-form-label">
                                <label for="post-form-content">Content:</label>
                            </div>
                            <div class="col-sm-10">
                                <textarea class="required form-control textarea-message" id="post-form-content"  name="content" rows="6" cols="30"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <div class="row">
                            <div class="col-sm-2 col-form-label">
                                <label for="post-form-content">Image:</label>
                            </div>
                            <div class="col-sm-10">
                                <input type="file" id="post-form-media" name="media" multiple class="file file-loading" data-show-preview="false">
                            </div>
                        </div>
                    </div>

                    <div class="col-12 d-flex justify-content-end align-items-center">
                        <button type="button" onclick="window.location='{{route('post.index')}}'" id="post-form-btn-cancel" class="btn btn-secondary">Cancel</button>
                        <button type="submit" id="post-form-btn-create" class="btn btn-success ml-2">Create</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<style>
    .form-group > label.error {
        display: block !important;
        text-transform: none;
    }

    .form-group input.valid ~ label.error,
    .form-group input[type="text"] ~ label.error,
    .form-group input[type="textarea"] ~ label.error,
    .form-group select ~ label.error { display: none !important; }
</style>

@section('specific_scripts')
	<!-- TinyMCE Plugin -->
    <script src="{{ asset('js/components/tinymce/tinymce.min.js') }}"></script>
@endsection

<script>
    $(document).ready( function(){

        tinymce.init({
            selector: '.textarea-message',
            menubar: false,
            setup: function(editor) {
                editor.on('change', function(e) {
                    editor.save();
                });
            }
        });

    });
</script>
@endsection

