<div class="entry col-12">
    <div class="grid-inner row align-items-center no-gutters">
        <div class="entry-image col-md-4">
            <a href="" data-lightbox="image"><img src="{{ $post_media }}" alt="Standard Post with Image"></a>
        </div>
        <div class="col-md-8 pl-md-4">
            <div class="entry-title title-sm">
                <h3><a href="{{ route('post.show', $post_id)}}">{{$post_title}}</a></h2>
            </div>
            <div class="entry-meta">
                <ul>
                    <li><i class="icon-calendar3"></i> {{ $post_created_date }}</li>
                    <li><a href="{{ route('post.show', $post_id)}}#comments"><i class="icon-comments"></i> 13 </a></li>
                    @if ($editable->toHtml() == "1")
                        <li><a href="{{ route('post.edit', $post_id)}}" title="Edit"><i class="icon-edit"></i></a></li>
                    @endif
                    @if ($editable->toHtml() == "1")
                        <li>
                            <form onSubmit="if(!confirm('Do you really want to delete Post?')){return false;}" method="post" action="{{ route('post.destroy', $post_id)}}" enctype="multipart/form-data" style="display: inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn" ><i class="icon-remove"></i></button>
                            </form>
                        </li>
                    @endif
                </ul>
            </div>
            <div class="entry-content">
                <p>{{ $slot }}</p>
                <a href="{{ route('post.show', $post_id)}}" class="more-link">Read More</a>
            </div>
        </div>
    </div>
</div>