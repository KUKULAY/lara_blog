@if (Auth::check())
    <div class="dropdown mx-3 mr-lg-0">
        <a href="" class="btn btn-secondary btn-sm dropdown-toggle" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="true"><i class="icon-user"></i></a>
        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
            <a class="dropdown-item text-left" href="{{ route('user.show', auth()->user()) }}">Profile</a>
            <a class="dropdown-item text-left" href="#">Messages <span
                    class="badge badge-pill badge-secondary float-right"
                    style="margin-top: 3px;">5</span></a>
            <a class="dropdown-item text-left" href="#">Settings</a>
            <div class="dropdown-divider"></div>
            <a href="{{ route('logout') }}" class="dropdown-item text-left" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                {{ __('Logout') }}<i class="icon-signout"></i>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </ul>
    </div>
    @else
    <a href="{{ route('login') }}"><i class="icon-line2-user mr-1 position-relative"
            style="top: 1px;"></i><span
            class="d-none d-sm-inline-block font-primary t500">Login</span></a>
    @endif