<form method="post" action="{{ $delete_url }}" enctype="multipart/form-data" style="display: inline">
    @csrf
    @method('DELETE')
    <button type="submit" class="button button-3d button-rounded button-red button-small"><i class="icon-trash2"></i>Delete</button>
</form>