@if(session('success'))
    <div class="style-msg successmsg">
        <div class="sb-msg">{!! session('success') !!}</div>
    </div>
@endif

@if (session('error'))
    <div class="style-msg errormsg">
        <div class="sb-msg">{!! session('error') !!}</div>
    </div>
@endif

@if (session('errors'))
    <div class="style-msg errormsg">
        <div class="sb-msg">
            @foreach(session('errors') as $error)
                {!! $error !!}<br/>
            @endforeach
        </div>
    </div>

@endif