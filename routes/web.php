<?php

use App\Mail\UserRegistered;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*  Using many middleware for route group

    Route::middleware(['first', 'second'])->group(function () {
        Route::get('/', function () {
            // Uses first & second Middleware
        });

        Route::get('user/profile', function () {
            // Uses first & second Middleware
        });
    }); 
*/

/* Using Prefix for route to do grouping

    Route::prefix('admin')->group(function () {
        Route::get('users', function () {
            // Matches The "/admin/users" URL
        });
    }); 
*/

/* Using Name for route to do grouping
    
    Route::name('admin.')->group(function () {
        Route::get('users', function () {
            // Route assigned name "admin.users"...
        })->name('users');
    });

*/

/* If you want to add all middleware+name+prefix to one group, it’s more readable to put them into an array:
    
    Route::group([
        'name' => 'admin.', 
        'prefix' => 'admin', 
        'middleware' => 'auth'
    ], function () {
        // ...
    }); 
*/

//Auth Routes
Auth::routes();


Route::get('/', function () {
    return redirect("post/newfeeds");
});

//Admin & User group
Route::group([
    'middleware' => 'auth',
], function() {

    //admin group
    Route::group([
        'middleware' => 'role:admin'
    ], function () {

        // URL: /admin/uri
        // Route name: admin.route-name
        Route::group([
            'name' => 'admin.',
            'prefix' => 'admin',
            'namespace' => 'Admin',
        ], function () {
            
            Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');
    
        });

        // URL: /user/uri
        // Route name: user.route-name
        Route::group([
            'name' => 'user.',
            'prefix' => 'user',
            'namespace' => 'User',
        ], function () {
            
            Route::post('/', 'UserController@store')->name('user.store');
            Route::get('/create', 'UserController@create')->name('user.create');
    
        });
        
        // URL: /role/uri
        // Route name: role.route-name
        Route::group([
            'name' => 'role.',
            'prefix' => 'role',
            'namespace' => 'Admin',
        ], function () {
            
            Route::get("/", "RoleController@index")->name('role.index'); 
            Route::post('/', 'RoleController@store')->name('role.store');
            Route::get('/create', 'RoleController@create')->name('role.create');
        
            Route::delete('/{role}/destroy', 'RoleController@destroy')->name('role.destroy');
            Route::get("/{role}/edit", "RoleController@edit")->name("role.edit");
            Route::patch('/{role}/update', 'RoleController@update')->name('role.update');
        });

    });

    //blogger group (auth users)
    Route::group([
        'name' => 'post.',
        'prefix' => 'post',
        'namespace' => 'Front',
    ], function () {
 
        // URL: /post/uri
        // Route name: post.create
        Route::get("/", "PostController@index")->name('post.index'); 
        Route::post('/', 'PostController@store')->name('post.store');
        Route::get('/create', 'PostController@create')->name('post.create');

        Route::delete('/{post}/destroy', 'PostController@destroy')->name('post.destroy');
        Route::get("/{post}/edit", "PostController@edit")->name("post.edit");
        Route::patch('/{post}/update', 'PostController@update')->name('post.update');

    });

    Route::group([
        'name' => 'user.',
        'prefix' => 'user',
        'namespace' => 'User',
    ], function () {

        //user profile
        Route::get("/{user}", "UserController@show")->name('user.show');

        //Admin and User itself only can edit User Details
        Route::middleware(["can:view,user"])->group(function(){
            Route::get("/{user}/edit", "UserController@edit")->name('user.edit');
            Route::patch("/{user}/update", "UserController@update")->name('user.update');
        });

        //Role Middleware
        Route::middleware(["role:admin"])->group(function(){
            Route::delete('/{user}/destroy', 'UserController@destroy')->name('user.destroy');
        });

    });

});

//Guest group
Route::group([
    'namespace' => 'Front', //folder name 
    ], function () {
        // URL: /post/uri
        // Route name: post.route-name
        //Mail::to('poepoe.dev.1991@gmail.com')->queue(new UserRegistered);

        Route::group([
            'name' => 'post.', //post.newfeeds
            'prefix' => 'post', //post/newfeeds
        ], function () {
            Route::get('/newfeeds', 'PostController@index')->name('post.newfeeds');
            Route::get('/{post}', 'PostController@show')->name('post.show'); 
        }); 
        
        Route::get('about/', 'AboutController@index')->name('about.index');
        Route::get('/overview', 'AboutController@index')->name('about.overview');
        
});

Route::get('/jobs/{jobs}', function($jobs){
    for ($i=0; $i < $jobs ; $i++) { 
        SomeJob::dispatch();
    }
});