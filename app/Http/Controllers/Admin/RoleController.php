<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return view('roles.index', ['roles'=>$roles]);
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|min:8|max:255'
        ]);
        
        if ($validator->passes()) {
            $params = $request->all();
            $name = Str::ucfirst($params['name']);
            $slug = Str::of(Str::lower($name))->slug('-');

            Role::create([
                'name' => $name,
                'slug' => $slug,
            ]);
            
            session()->flash('success', 'Role created successfully!');

            return redirect()->route('role.index');
        }
        
        session()->flash('errors', $validator->errors()->all());
        return back();
    }

    public function update(Role $role, Request $request)
    {
        
        $validator = \Validator::make($request->all(), [
            'name' => 'required|min:8|max:255'
        ]);
        
        if ($validator->passes()) {
            $params = $request->all();
            $role->name = Str::ucfirst($params['name']);
            $role->slug = Str::of(Str::lower($role->name))->slug('-');

            $role->save();
            
            session()->flash('success', 'Role updated successfully!');

            return redirect()->route('role.index');
        }
        
        session()->flash('errors', $validator->errors()->all());
        return back();
    }

    public function destroy(Role $role ,Request $request){

        $role->delete();

        $request->session()->flash('success', 'Role deleted successfully!');

        return back();
    }
}
