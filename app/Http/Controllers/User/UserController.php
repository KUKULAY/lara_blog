<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('users.index', ['users'=>$users]);
    }

    public function show(User $user)
    {
        return view('users.profile', ['user' => $user]);
    }

    public function create()
    {
        return view('users.create');
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|min:8|max:255',
            'email'    => 'required|email',
            'avatar' => 'mimes:jpg,jpeg,png,bmp,tiff |max:2048',
            'password' => [
                'required',
                'string',
                'min:6',             // must be at least 10 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
                'regex:/[@$!%*#?&]/', // must contain a special character
            ],
            'password_confirm' => 'required|same:password' //must same with passwrod
        ]);
        
        if ($validator->passes()) {
            $params = $request->all();
            $name = $params['name'];
            $email = $params['email'];
            $user_bio = $params['user_bio'];
            $avatar = '';

            if(request('avatar')){
                $avatar = $request->file('avatar')->store('uploaded_images');
            }

            if(request('password')){
                $password = $params['password'];
            }

            $user = User::create([
                'name' => $name,
                'email' => $email,
                'user_bio' => $user_bio,
                'avatar' => $avatar,
                'password' => $password
            ]);
            
            session()->flash('success', 'Profile created successfully!');

            return redirect()->route('admin.dashboard');
        }
        
        session()->flash('errors', $validator->errors()->all());
        return back();
    }

    public function edit(User $user)
    {
        $this->authorize("update", $user);
        return view('users.edit',['user'=>$user]);
    }

    public function update(User $user, Request $request)
    {
        $this->authorize("update", $user);
        $validator = \Validator::make($request->all(), [
            'name' => 'required|min:8|max:255',
            'email'    => 'required|email',
            'avatar' => 'mimes:jpg,jpeg,png,bmp,tiff |max:2048',
            'password' => [
                'required',
                'string',
                'min:6',             // must be at least 10 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
                'regex:/[@$!%*#?&]/', // must contain a special character
            ],
            'password_confirm' => 'required|same:password' //must same with passwrod
        ]);
        
        if ($validator->passes()) {
            $params = $request->all();
            //dd($params);
            $user->name = $params['name'];
            $user->email = $params['email'];
            $user->user_bio = $params['user_bio'];
            
            if(request('avatar')){
                $user->avatar = $request->file('avatar')->store('uploaded_images');
            }

            if(request('password')){
                $user->password = $params['password'];
            }

            $user->save();
            
            session()->flash('success', 'Profile updated successfully!');

            return redirect()->route('post.index');
        }
        
        session()->flash('errors', $validator->errors()->all());
        return back();
    }

    public function destroy(User $user ,Request $request){

        $this->authorize("delete", $user);

        $user->delete();

        $request->session()->flash('success', 'User deleted successfully!');

        return back();
    }
}
