<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    public function index(){
        //dd( storage_path('app/public/') );
        //"/Users/poedarlitheint/Sites/working/blog2020/storage/app/public/"
        
        //public_path('storage') => storage_path('app/public')
        //dd(public_path('storage') );
        //"/Users/poedarlitheint/Sites/working/blog2020/public/storage"
        
        //dd($posts); 
        /* DB::enableQueryLog();
        $users = Post::select("*")->get();
        $quries = DB::getQueryLog();
        dd($quries); */
        $posts = Post::paginate(9);
        return view("posts.blog-grid", ['posts' => $posts]);
    }

    public function show(Post $post)
    {
        return view("posts.blog-single", ['post' => $post]);
    }

    public function create()
    {
        return view("posts.create");
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'title' => 'required|min:8|max:255',
            'content' => 'required',
            'media' => 'required|mimes:jpg,jpeg,png,bmp,tiff |max:4096',
        ]);
        
        if ($validator->passes()) {
            $params = $request->all();

            if(request('media')){
                //$params['media'] = request('media')->store('images');
                $params['media'] = $request->file('media')->store('uploaded_images');
            }

            $result = auth()->user()->posts()->create($params);
            
            session()->flash('success', 'Post created successfully!');

            return redirect()->route('post.index');
        }
        
        session()->flash('errors', $validator->errors()->all());
        return back();
    }

    public function destroy(Post $post ,Request $request){

        $this->authorize("delete", $post);

        $post->delete();

        $request->session()->flash('success', 'Post deleted successfully!');

        return back();
    }

    public function edit(Post $post)
    {
        $this->authorize("view", $post);

        return view('posts.edit',['post'=>$post]);
    }

    public function update(Post $post, Request $request)
    {
        $this->authorize("update", $post);

        $validator = \Validator::make($request->all(), [
            'title' => 'required|min:8|max:255',
            'content' => 'required',
            'media' => 'mimes:jpg,jpeg,png,bmp,tiff |max:4096',
        ]);
        
        if ($validator->passes()) {
            $params = $request->all();
            //dd($params['title']);
            $post->title = $params['title'];
            $post->content = $params['content'];

            if(request('media')){
                $post->media = $request->file('media')->store('uploaded_images');
            }

            $post->save();
            
            session()->flash('success', 'Post updated successfully!');

            return redirect()->route('post.index');
        }
        
        session()->flash('errors', $validator->errors()->all());
        return back();
    }

}
