<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;

class AboutController extends Controller
{
    public function index()
    {
        return view('about.overview');
    }
}
